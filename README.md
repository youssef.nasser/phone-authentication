# Task

    login by mobile number with verification by firebase sdk

# Solution

This project demonstrates phone authentication using Firebase SDK. Users can enter their phone number, receive an OTP, and verify it to authenticate.

## Prerequisites

Before running the project, ensure you have the following:

- Firebase project set up with authentication enabled.
- Firebase configuration details (`apiKey`, `authDomain`, `projectId`, etc.) in the script section of your HTML file.
- The necessary Firebase SDK scripts included in your HTML file.

## Usage

1. **Open in Browser:**
    Open the HTML file in your preferred web browser.

2. **Fill in Details:**
   - Enter your phone number.
   - Select the country code.

3. **Initiate Authentication:**
   - Click the "Send OTP" button to start the phone authentication process.

4. **Verify OTP:**
   - Enter the received OTP in the verification code input.

5. **Complete Authentication:**
   - Click the "Verify Code" button to complete the authentication.

## Firebase Configuration

Ensure to replace the placeholder Firebase configuration in the script section with your project details:

```javascript
const firebaseConfig = {
    apiKey: "YOUR_API_KEY",
    authDomain: "YOUR_AUTH_DOMAIN",
    projectId: "YOUR_PROJECT_ID",
    storageBucket: "YOUR_STORAGE_BUCKET",
    messagingSenderId: "YOUR_MESSAGING_SENDER_ID",
    appId: "YOUR_APP_ID",
    measurementId: "YOUR_MEASUREMENT_ID"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
