<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <title>Phone Authentication</title>
</head>

<body>
    <form>
        <h1>Firebase Phone Verification </h1>
        <div class="formcontainer">
            <hr />
            <div class="container">
                <label for="uname"><strong>Phone Number </strong></label>
                <select id="countryCode" onchange="addCountryCode()" required>
                    <option value="+966">KSA (+966)</option>
                    <option value="+20">EGYPT (+20)</option>
                </select>
                <input type="text" id="number" placeholder="Enter phone number" name="uname" required>
            </div>
            <br>
            <div id="recaptcha-container"></div>
            <br>
            <button type="button" onclick="phoneAuth();">Send OTP</button>
        </div>
    </form>

    <form>
        <div class="formcontainer">
            <hr />
            <div class="container">
                <input type="text" id="verificationCode" placeholder="Enter verification code">
            </div>
            <button type="button" onclick="codeverify();">Verify code</button>
        </div>
    </form>

    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-firestore.js"></script>

    <script>
        const firebaseConfig = {
            apiKey: "AIzaSyBWDNbWePqoyDDLljKaOjKS3_uEH4Phn00",
            authDomain: "login-15e7e.firebaseapp.com",
            projectId: "login-15e7e",
            storageBucket: "login-15e7e.appspot.com",
            messagingSenderId: "985155340098",
            appId: "1:985155340098:web:86f7ca355b2fb26a9caadb",
            measurementId: "G-E3L3C59RFN"
        };

        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        window.onload = function () {
            render();
        };

        function render() {
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
            recaptchaVerifier.render();
        }

        function addCountryCode() {
            var countryCode = document.getElementById("countryCode").value;
            document.getElementById("number").value = countryCode;
        }

        function phoneAuth() {
            const number = document.getElementById('number').value;

            firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                coderesult = confirmationResult;
                console.log(coderesult);
                alert("Message sent");
            }).catch(function (error) {
                alert(error.message);
            });
        }

        function codeverify() {
            const code = document.getElementById('verificationCode').value;

            coderesult.confirm(code).then(function (result) {
                alert("Successfully registered");
                const user = result.user;
                console.log(user);
            }).catch(function (error) {
                alert(error.message);
            });
        }
    </script>
</body>

</html>
